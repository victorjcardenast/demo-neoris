-- Crear el usuario 'neoris-demo' con la contraseña 'neoris-demo'
CREATE ROLE "neoris-demo" WITH
	LOGIN
	SUPERUSER
	CREATEDB
	CREATEROLE
	INHERIT
	NOREPLICATION
	CONNECTION LIMIT -1
	PASSWORD 'neoris-demo';

CREATE DATABASE neorisdb
    WITH
    OWNER = "neoris-demo"
    ENCODING = 'UTF8'
    CONNECTION LIMIT = -1
    IS_TEMPLATE = False;

GRANT ALL ON DATABASE neorisdb TO PUBLIC;

CREATE SCHEMA neoris
    AUTHORIZATION postgres;

GRANT ALL ON SCHEMA neoris TO PUBLIC;
