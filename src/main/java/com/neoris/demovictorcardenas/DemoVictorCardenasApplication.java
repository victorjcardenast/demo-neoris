package com.neoris.demovictorcardenas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoVictorCardenasApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoVictorCardenasApplication.class, args);
	}

}
