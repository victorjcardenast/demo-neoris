package com.neoris.demovictorcardenas.reports.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MovementReportRequestDto {

    private LocalDateTime fromDate;
    private LocalDateTime toDate;
    private Long account;

}
