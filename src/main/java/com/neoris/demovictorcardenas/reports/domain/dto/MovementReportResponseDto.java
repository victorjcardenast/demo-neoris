package com.neoris.demovictorcardenas.reports.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MovementReportResponseDto {

    private Date transactionDate;
    private String client;
    private Long accountNumber;
    private String accountType;
    private Double initialMount;
    private Boolean status;
    private String movement;
    private Double currentBalance;

}
