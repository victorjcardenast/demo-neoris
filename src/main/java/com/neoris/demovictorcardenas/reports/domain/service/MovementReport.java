package com.neoris.demovictorcardenas.reports.domain.service;

public interface MovementReport<T> {

    public T movementByDatesAndAccount(T dto);
}
