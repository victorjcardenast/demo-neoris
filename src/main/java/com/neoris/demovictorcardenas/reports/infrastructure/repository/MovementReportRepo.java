package com.neoris.demovictorcardenas.reports.infrastructure.repository;

import com.neoris.demovictorcardenas.movement.infrastructure.entities.AccountTransactionEntity;
import com.neoris.demovictorcardenas.reports.domain.dto.MovementReportRequestDto;
import com.neoris.demovictorcardenas.reports.domain.dto.MovementReportResponseDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface MovementReportRepo extends JpaRepository<AccountTransactionEntity, Long> {

    @Query("SELECT " +
            "new com.neoris.demovictorcardenas.reports.domain.dto.MovementReportResponseDto( " +
            "at.dateMovement, " +
            "cl.names, " +
            "ac.accountNumber, " +
            "ac.accountType, " +
            "ac.initialBalance, " +
            "ac.status, " +
            "at.movementType, " +
            "at.balance " +
            ") " +
            "FROM AccountTransactionEntity at " +
            "   JOIN at.account ac " +
            "   JOIN ac.clients cl " +
            "WHERE at.dateMovement BETWEEN :fromDate AND :toDate " +
            "   AND ac.accountNumber = :account " +
            "")
    public List<MovementReportResponseDto> findByAccountAndFromToDate(
            @Param("fromDate") Date fromDate
            ,@Param("toDate") Date toDate
            ,@Param("account") Long account
    );
}
