package com.neoris.demovictorcardenas.reports.infrastructure.controller;

import com.neoris.demovictorcardenas.client.application.dto.BasicResponse;
import com.neoris.demovictorcardenas.client.application.dto.ResponseWithObject;
import com.neoris.demovictorcardenas.reports.domain.dto.MovementReportRequestDto;
import com.neoris.demovictorcardenas.reports.domain.service.MovementReport;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Tag(name = "Movimiento Reporte", description = "Endpoint de reporte movimientos de cuenta")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/transactionReport")
public class MovementReportController {

    @Autowired
    private MovementReport mvReportService;

    @PostMapping("/movementReport")
    public ResponseEntity save(@RequestBody MovementReportRequestDto account) {
        var response = mvReportService.movementByDatesAndAccount(account);
        if(response instanceof BasicResponse) {
            return ResponseEntity.status(((BasicResponse) response).getCode()).body(response);
        }
        return ResponseEntity.status(((ResponseWithObject) response).getCode()).body(response);
    }
}
