package com.neoris.demovictorcardenas.reports.infrastructure.service;

import com.neoris.demovictorcardenas.client.application.dto.BasicResponse;
import com.neoris.demovictorcardenas.client.application.dto.ResponseWithObject;
import com.neoris.demovictorcardenas.reports.domain.dto.MovementReportRequestDto;
import com.neoris.demovictorcardenas.reports.domain.dto.MovementReportResponseDto;
import com.neoris.demovictorcardenas.reports.domain.service.MovementReport;
import com.neoris.demovictorcardenas.reports.infrastructure.repository.MovementReportRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class MovementReportImpl<T> implements MovementReport {

    @Autowired
    private MovementReportRepo mrRepository;

    @Override
    public T movementByDatesAndAccount(Object dto) {

        List<MovementReportResponseDto> response = new ArrayList<>();
        Date fromDate = new Date();
        Date toDate = new Date();

        if(!(dto instanceof MovementReportRequestDto)) {
            return (T) BasicResponse.builder()
                    .status("Error")
                    .code(400)
                    .message("Error estructura request no compatible. ")
                    .build();
        }

        fromDate = Date.from((((MovementReportRequestDto) dto).getFromDate()).atZone(ZoneId.systemDefault()).toInstant());
        toDate = Date.from((((MovementReportRequestDto) dto).getToDate()).atZone(ZoneId.systemDefault()).toInstant());
        try {
            response = mrRepository.findByAccountAndFromToDate(
                    fromDate,
                    toDate,
                    ((MovementReportRequestDto) dto).getAccount());

            return (T) ResponseWithObject.builder()
                    .status("Ok")
                    .code(200)
                    .message("Consulta realizada.")
                    .object(response)
                    .build();
        } catch (Exception e) {
            System.out.print(e.getMessage());
            return (T) BasicResponse.builder()
                    .status("Error")
                    .code(400)
                    .message("Error al realizar consulta. ")
                    .build();
        }
    }
}
