package com.neoris.demovictorcardenas.movement.application.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TransactionResponseDto {

    private Long transactionId;
    private Date dateTransaction;
    private String clientName;
    private Long accountNumber;
    private String accountType;
    private Double initialAmount;
    private Double amount;
    private Double availableBalance;
    private Boolean status;

}
