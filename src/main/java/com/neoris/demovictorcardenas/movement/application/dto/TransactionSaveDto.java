package com.neoris.demovictorcardenas.movement.application.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TransactionSaveDto {

    private Long accountNumber;
    private String accountType;
    private Double amount;
    private Boolean status;
    private String movementType;

}
