package com.neoris.demovictorcardenas.movement.infrastructure.entities;

import com.neoris.demovictorcardenas.account.infrastructure.entities.AccountEntity;
import jakarta.persistence.*;
import lombok.*;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Account_Movements", schema = "neoris")
public class AccountTransactionEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "date_movement")
    private Date dateMovement;

    @Column(name = "movement_type", length = 20)
    private String movementType;

    @Column
    private Double amount;

    @Column
    private Double initialBalance;

    @Column
    private Double balance;

    @Column
    private Boolean status;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id")
    @ToString.Exclude
    private AccountEntity account;
}
