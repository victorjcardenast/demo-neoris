package com.neoris.demovictorcardenas.movement.infrastructure.repositories;

import com.neoris.demovictorcardenas.movement.infrastructure.entities.AccountTransactionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AccountMovementRepository extends JpaRepository<AccountTransactionEntity, Long> {

}
