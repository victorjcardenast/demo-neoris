package com.neoris.demovictorcardenas.movement.infrastructure.controllers;

import com.neoris.demovictorcardenas.account.application.dto.AccountSaveDto;
import com.neoris.demovictorcardenas.client.application.dto.BasicResponse;
import com.neoris.demovictorcardenas.client.application.dto.ResponseWithObject;
import com.neoris.demovictorcardenas.movement.application.dto.TransactionSaveDto;
import com.neoris.demovictorcardenas.movement.domain.services.TransactionCrudService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Tag(name = "Movimiento", description = "Endpoint de movimientos de cuenta")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/transaction")
public class TransactionController {

    @Autowired
    private TransactionCrudService trService;

    @PostMapping("/save")
    public ResponseEntity save(@RequestBody TransactionSaveDto account) {
        var response = trService.save(account);
        if(response instanceof BasicResponse) {
            return ResponseEntity.status(((BasicResponse) response).getCode()).body(response);
        }
        return ResponseEntity.status(((ResponseWithObject) response).getCode()).body(response);
    }
}
