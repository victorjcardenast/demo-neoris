package com.neoris.demovictorcardenas.movement.infrastructure.services;

import com.neoris.demovictorcardenas.account.application.dto.AccountSaveDto;
import com.neoris.demovictorcardenas.account.infrastructure.entities.AccountEntity;
import com.neoris.demovictorcardenas.account.infrastructure.repositories.AccountRepository;
import com.neoris.demovictorcardenas.client.application.dto.BasicResponse;
import com.neoris.demovictorcardenas.client.application.dto.ResponseWithObject;
import com.neoris.demovictorcardenas.movement.application.dto.TransactionResponseDto;
import com.neoris.demovictorcardenas.movement.application.dto.TransactionSaveDto;
import com.neoris.demovictorcardenas.movement.domain.services.TransactionCrudService;
import com.neoris.demovictorcardenas.movement.infrastructure.entities.AccountTransactionEntity;
import com.neoris.demovictorcardenas.movement.infrastructure.repositories.AccountMovementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
public class TransactionServiceImpl<T> implements TransactionCrudService {

    @Autowired
    private AccountMovementRepository acmRepository;

    @Autowired
    private AccountRepository acRepository;

    @Override
    public T save(Object tr) {
        Optional<AccountEntity> acEntity = Optional.empty();
        AccountTransactionEntity actEntity = new AccountTransactionEntity();
        TransactionResponseDto response = new TransactionResponseDto();
        Double amount = 0d;
        Double currentBalance = 0d;

        if(tr instanceof TransactionSaveDto) {
            acEntity = acRepository.findByAccountNumber(((TransactionSaveDto) tr).getAccountNumber());

            if (acEntity.isEmpty()) {
                return (T) BasicResponse.builder()
                        .code(404)
                        .message("Cuenta no encontrada")
                        .status("Not found")
                        .build();
            }

            if (((TransactionSaveDto) tr).getAmount() == 0) {
                return (T) BasicResponse.builder()
                        .code(400)
                        .message("Monto debe ser mayor o menor a CERO")
                        .status("Error")
                        .build();
            }

            if (!(((TransactionSaveDto) tr).getMovementType().toLowerCase().equals("deposito") ||
                    ((TransactionSaveDto) tr).getMovementType().toLowerCase().equals("retiro"))) {
                return (T) BasicResponse.builder()
                        .code(400)
                        .message("Transacción no permitida")
                        .status("Error")
                        .build();
            }

            amount = ((TransactionSaveDto) tr).getAmount();
            if(((TransactionSaveDto) tr).getMovementType().toLowerCase().equals("retiro")) {
                amount = (-1) * amount;
            }

            currentBalance = acEntity.get().getCurrentBalance();

            if ((currentBalance + amount) < 0) {
                return (T) BasicResponse.builder()
                        .code(400)
                        .message("Saldo insificiente para retirar")
                        .status("Error")
                        .build();
            }

            actEntity = AccountTransactionEntity.builder()
                    .dateMovement(new Date())
                    .movementType(amount < 0 ?
                            "Retiro de " + ((-1) * amount) :
                            "Deposito de " + amount)
                    .initialBalance(currentBalance)
                    .amount(amount)
                    .balance(currentBalance + amount)
                    .status(true)
                    .account(acEntity.get())
                    .build();
            actEntity = acmRepository.save(actEntity);

            acEntity.get().setCurrentBalance(actEntity.getBalance());
            acRepository.save(acEntity.get());

            response = TransactionResponseDto.builder()
                    .transactionId(actEntity.getId())
                    .dateTransaction(actEntity.getDateMovement())
                    .clientName(acEntity.get().getClients().getNames())
                    .accountNumber(acEntity.get().getAccountNumber())
                    .accountType(acEntity.get().getAccountType())

                    .initialAmount(currentBalance)
                    .amount(actEntity.getAmount())
                    .availableBalance(acEntity.get().getCurrentBalance())

                    .status(actEntity.getStatus())
                    .build();

            return (T) ResponseWithObject.builder()
                    .code(200)
                    .message("Transacción exitosa")
                    .status("Ok")
                    .object(response)
                    .build();
        } else {
            return (T) BasicResponse.builder()
                    .code(400)
                    .message("Error al guardar transacción")
                    .status("Error")
                    .build();
        }
    }
}
