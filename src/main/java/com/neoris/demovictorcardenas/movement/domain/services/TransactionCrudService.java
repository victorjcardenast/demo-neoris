package com.neoris.demovictorcardenas.movement.domain.services;

public interface TransactionCrudService<T> {

    public T save(T object);
}
