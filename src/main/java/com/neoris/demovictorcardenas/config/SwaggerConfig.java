package com.neoris.demovictorcardenas.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.servers.Server;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfig {

    @Bean
    public OpenAPI openApi() {
        Server devServer = new Server();
        devServer.setUrl("");
        devServer.setDescription("Server URL in Development environment");

        Contact contact = new Contact();
        contact.setEmail("victorjcardenast@gmail.com");
        contact.setName("Victor Cardenas");
        contact.setUrl("");

        License mitLicense = new License().name("Víctor Cárdenas NEORIS 2024")
                .url("");

        Info info = new Info()
                .title("NEORIS Demo API")
                .version("1.0")
                .description("Servicios para sistema NEORIS.")
                ;

        return new OpenAPI().info(info);

    }

}
