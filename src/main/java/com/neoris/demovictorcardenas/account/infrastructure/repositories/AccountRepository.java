package com.neoris.demovictorcardenas.account.infrastructure.repositories;

import com.neoris.demovictorcardenas.account.infrastructure.entities.AccountEntity;
import com.sun.jdi.LongValue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AccountRepository extends JpaRepository<AccountEntity, LongValue> {

    @Query("FROM AccountEntity acc " +
            "WHERE acc.accountNumber = :accountNumber ")
    public Optional<AccountEntity> findByAccountNumber(@Param("accountNumber") Long accountNumber);
}
