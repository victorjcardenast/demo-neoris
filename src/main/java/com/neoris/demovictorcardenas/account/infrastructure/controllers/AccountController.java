package com.neoris.demovictorcardenas.account.infrastructure.controllers;

import com.neoris.demovictorcardenas.account.application.dto.AccountSaveDto;
import com.neoris.demovictorcardenas.account.application.dto.AccountUpdateDto;
import com.neoris.demovictorcardenas.account.domain.services.AccountService;
import com.neoris.demovictorcardenas.client.application.dto.BasicResponse;
import com.neoris.demovictorcardenas.client.application.dto.ResponseWithObject;
import com.neoris.demovictorcardenas.client.domain.models.ClientModel;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Tag(name = "Cuentas", description = "Crud Cuentas")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/account")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @PostMapping("/save")
    public ResponseEntity save(@RequestBody AccountSaveDto account) {
        var response = accountService.save(account);
        return ResponseEntity.status(((BasicResponse)response).getCode()).body(response);
    }

    @PutMapping("/update")
    public ResponseEntity update(@RequestBody AccountUpdateDto account) {
        var response = accountService.update(account);
        if(response instanceof BasicResponse) {
            return ResponseEntity.status(((BasicResponse) response).getCode()).body(response);
        }
        return ResponseEntity.status(((ResponseWithObject) response).getCode()).body(response);
    }

}
