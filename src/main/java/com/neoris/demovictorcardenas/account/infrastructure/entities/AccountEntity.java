package com.neoris.demovictorcardenas.account.infrastructure.entities;

import com.neoris.demovictorcardenas.client.infrastructure.entities.ClientEntity;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "account_db", schema = "neoris")
public class AccountEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "account_number", nullable = false, unique = true)
    private Long accountNumber;

    @Column(name = "account_type", length = 20, nullable = false)
    private String accountType;

    @Column(name = "initial_balance")
    private Double initialBalance;

    @Column(name = "current_balance")
    private Double currentBalance;

    @Column
    private Boolean status;

//    @ManyToMany(fetch = FetchType.EAGER)
//    @JoinTable(name = "client_account", schema = "neoris",
//    joinColumns = @JoinColumn(name = "account_id"),
//    inverseJoinColumns = @JoinColumn(name = "client_id"))
    @ManyToOne(fetch = FetchType.LAZY)
    @ToString.Exclude
    private ClientEntity clients;
}
