package com.neoris.demovictorcardenas.account.infrastructure.servicesimpl;

import com.neoris.demovictorcardenas.account.application.dto.AccountSaveDto;
import com.neoris.demovictorcardenas.account.application.dto.AccountUpdateDto;
import com.neoris.demovictorcardenas.account.domain.model.AccountModel;
import com.neoris.demovictorcardenas.account.domain.services.AccountService;
import com.neoris.demovictorcardenas.account.infrastructure.entities.AccountEntity;
import com.neoris.demovictorcardenas.account.infrastructure.repositories.AccountRepository;
import com.neoris.demovictorcardenas.client.application.dto.BasicResponse;
import com.neoris.demovictorcardenas.client.application.dto.ResponseWithObject;
import com.neoris.demovictorcardenas.client.infrastructure.entities.ClientEntity;
import com.neoris.demovictorcardenas.client.infrastructure.entities.PersonEntity;
import com.neoris.demovictorcardenas.client.infrastructure.repositories.ClientRepository;
import com.neoris.demovictorcardenas.client.infrastructure.repositories.PersonRepository;
import com.neoris.demovictorcardenas.client.infrastructure.services.ClientCrudServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Service
public class AccountServiceImpl<T> implements AccountService {

    Logger logger = LoggerFactory.getLogger(AccountService.class);

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Override
    public T save(Object account) {
        Optional<PersonEntity> prEntity = Optional.empty();
        Optional<AccountEntity> acEntity = Optional.empty();
        ClientEntity clEntity = new ClientEntity();

        if(account instanceof AccountSaveDto) {
            prEntity = personRepository.findByNames(((AccountSaveDto) account).getNames());
            if(prEntity.isEmpty()) {
                return (T) BasicResponse.builder()
                        .code(404)
                        .message("Cliente no encontrado")
                        .status("Not fount")
                        .build();
            }

            try {
                clEntity = (ClientEntity) prEntity.get();
                acEntity = Optional.ofNullable(AccountEntity.builder()
                                .accountNumber(Long.parseLong(((AccountSaveDto) account).getAccountNumber()))
                                .accountType(((AccountSaveDto) account).getAccountType())
                                .initialBalance(((AccountSaveDto) account).getInitialBalance())
                                .currentBalance(((AccountSaveDto) account).getCurrentBalance())
                                .status(true)
                                .clients(clEntity)
                        .build());
                accountRepository.save(acEntity.get());
                logger.info("Cuenta creada exitosamente");

                return (T) BasicResponse.builder()
                        .code(201)
                        .message("Cuenta creada exitosamente")
                        .status("Ok")
                        .build();
            } catch (Exception e){
                return (T) BasicResponse.builder()
                        .code(400)
                        .message("Error al crear cuenta: " + e.getMessage())
                        .status("Not fount")
                        .build();
            }

        } else {
            return (T) BasicResponse.builder()
                    .code(400)
                    .message("Error estructura cuenta: ")
                    .status("Not fount")
                    .build();
        }
    }

    @Override
    public T update(Object account) {
        Optional<AccountEntity> acEntity = Optional.empty();
        if (account instanceof AccountUpdateDto) {

            try {
                acEntity = accountRepository.findByAccountNumber(((AccountUpdateDto) account).getAccountNumber());
                if(acEntity.isEmpty()) {
                    return (T) BasicResponse.builder()
                            .code(404)
                            .message("Error cuenta no encontrada: ")
                            .status("Not found")
                            .build();
                }

                acEntity.get().setCurrentBalance(((AccountUpdateDto) account).getCurrentBalance());
                accountRepository.save(acEntity.get());

                return (T) ResponseWithObject.builder()
                        .code(200)
                        .message("Cuenta actualizada: ")
                        .status("Ok")
                        .object(acEntity.get())
                        .build();
            } catch (Exception e) {
                return (T) BasicResponse.builder()
                        .code(400)
                        .message("Error estructura cuenta: ")
                        .status("Error")
                        .build();
            }

        } else {
            return (T) BasicResponse.builder()
                    .code(400)
                    .message("Error al actualizar cuenta: ")
                    .status("Error")
                    .build();
        }

    }

    @Override
    public AccountModel findById(AccountModel account) {
        return null;
    }

    @Override
    public Boolean delete(Long id) {
        return null;
    }

    @Override
    public AccountModel findAll() {
        return null;
    }
}
