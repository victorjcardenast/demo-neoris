package com.neoris.demovictorcardenas.account.domain.services;

import com.neoris.demovictorcardenas.account.domain.model.AccountModel;

public interface AccountService<T> {

    public T save(T account);
    public T update(T account);
    public AccountModel findById(AccountModel account);
    public Boolean delete(Long id);
    public AccountModel findAll();

}
