package com.neoris.demovictorcardenas.account.domain.model;

import com.neoris.demovictorcardenas.client.domain.models.ClientModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccountModel {

    private Long id;
    private Long accountNumber;
    private String accountType;
    private Double initialBalance;
    private Double currentBalance;
    private Boolean status;

    ClientModel client;

}
