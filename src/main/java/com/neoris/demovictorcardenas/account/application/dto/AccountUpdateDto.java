package com.neoris.demovictorcardenas.account.application.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccountUpdateDto {

    private Long accountNumber;
    private Double currentBalance;

}
