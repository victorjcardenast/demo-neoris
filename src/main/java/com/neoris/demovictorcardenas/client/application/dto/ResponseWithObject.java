package com.neoris.demovictorcardenas.client.application.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResponseWithObject<T> {

    private Integer code;
    private String message;
    private String status;
    private T object;
}
