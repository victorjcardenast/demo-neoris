package com.neoris.demovictorcardenas.client.application.dto;

import java.util.Date;

public class PersonClientDto {

    private Long id;
    private String dni;
    private String names;
    private String surnames;
    private Date birthdayDate;
    private Integer age;
    private String address;
    private String telephoneNumber;
}
