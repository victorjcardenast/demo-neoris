package com.neoris.demovictorcardenas.client.application.dto;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientUpdateDto {

    private Long id;
    private String dni;
    private String names;
    private Integer age;
    private String address;
    private String telephoneNumber;

    private String password;
    private Boolean status;
}
