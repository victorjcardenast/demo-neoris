package com.neoris.demovictorcardenas.client.application.dto;

import jakarta.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientSaveDto {

    private String dni;

    private String names;

    private String address;

    private Integer age;

    private String telephoneNumber;

    private String password;

    private Boolean status;


}
