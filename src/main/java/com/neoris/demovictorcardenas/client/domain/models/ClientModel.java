package com.neoris.demovictorcardenas.client.domain.models;

import lombok.*;

@Data
//@Builder
//@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper=false)
public class ClientModel extends PersonModel {

    private Long id;
    private String name;
    private String password;
    private Boolean status;

    public ClientModel() {
        super();
    }

    public ClientModel(
            String dni,
            String names,
            String address,
            String telephoneNumber,
            String name,
            String password
    ) {
        super();
        this.setDni(dni);
        this.setNames(names);
        this.setAddress(address);
        this.setTelephoneNumber(telephoneNumber);
        this.name = name;
        this.password = password;
        this.status = true;
    }

    public ClientModel(
            Long id,
            String dni,
            String names,
            String address,
            String telephoneNumber,
            String name,
            String password,
            Boolean status
    ) {
        super();
        this.setId(id);
        this.setDni(dni);
        this.setNames(names);
        this.setAddress(address);
        this.setTelephoneNumber(telephoneNumber);
        this.name = name;
        this.password = password;
        this.status = status;
    }

}
