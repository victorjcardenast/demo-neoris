package com.neoris.demovictorcardenas.client.domain.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PersonModel {

    private Long id;
    private String dni;
    private String names;
    private Integer age;
    private String address;
    private String telephoneNumber;

}
