package com.neoris.demovictorcardenas.client.domain.services;

import com.neoris.demovictorcardenas.client.application.dto.ClientUpdateDto;
import com.neoris.demovictorcardenas.client.domain.models.ClientModel;
import com.neoris.demovictorcardenas.client.domain.models.PersonModel;

import java.util.List;

public interface ClientCrudService<T> {

    public T save(Object model);

    public T update(Object model);

    public T delete(Long id);

    public T findById(Long id);

    public List<PersonModel> findAll();


    public T findByNames(String names);
}
