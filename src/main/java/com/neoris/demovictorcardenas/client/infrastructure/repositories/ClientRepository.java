package com.neoris.demovictorcardenas.client.infrastructure.repositories;

import com.neoris.demovictorcardenas.client.infrastructure.entities.ClientEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends JpaRepository<ClientEntity, Long> {
}
