package com.neoris.demovictorcardenas.client.infrastructure.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.neoris.demovictorcardenas.client.application.dto.BasicResponse;
import com.neoris.demovictorcardenas.client.application.dto.ClientSaveDto;
import com.neoris.demovictorcardenas.client.application.dto.ClientUpdateDto;
import com.neoris.demovictorcardenas.client.application.dto.ResponseWithObject;
import com.neoris.demovictorcardenas.client.infrastructure.entities.ClientEntity;
import com.neoris.demovictorcardenas.client.infrastructure.entities.PersonEntity;
import com.neoris.demovictorcardenas.client.infrastructure.repositories.ClientRepository;
import com.neoris.demovictorcardenas.client.infrastructure.repositories.PersonRepository;
import com.neoris.demovictorcardenas.client.domain.models.ClientModel;
import com.neoris.demovictorcardenas.client.domain.models.PersonModel;
import com.neoris.demovictorcardenas.client.domain.services.ClientCrudService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Service
public class ClientCrudServiceImpl<T> implements ClientCrudService {

    Logger logger = LoggerFactory.getLogger(ClientCrudServiceImpl.class);

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    ClientRepository clientRepository;

    @Override
    public T save(Object model) {
        ClientEntity clEntity = new ClientEntity();

        if (model instanceof ClientSaveDto) {
            clEntity.setDni(((ClientSaveDto) model).getDni());
            clEntity.setNames(((ClientSaveDto) model).getNames());
            clEntity.setAddress(((ClientSaveDto) model).getAddress());
            clEntity.setAge(((ClientSaveDto) model).getAge());
            clEntity.setTelephoneNumber(((ClientSaveDto) model).getTelephoneNumber());

            clEntity.setName(((ClientSaveDto) model).getNames().toLowerCase().replace(" ",""));
            clEntity.setPassword(((ClientSaveDto) model).getPassword());
            clEntity.setStatus(((ClientSaveDto) model).getStatus());

            try {
                clEntity = clientRepository.save(clEntity);
                logger.info("Registro ingresado correctamente.");
                return (T) BasicResponse.builder()
                        .code(201)
                        .status("Ok")
                        .message("Registro ingresado correctamente")
                        .build();
            } catch (Exception e) {
                logger.error("Error al ingresar.");
                return (T) BasicResponse.builder()
                        .code(400)
                        .status("Error")
                        .message("Error al registrar")
                        .build();
            }

        } else {
            logger.error("Error al ingresado.");
            return (T) BasicResponse.builder()
                    .code(400)
                    .status("Error")
                    .message("Error al registrar")
                    .build();
        }
    }

    @Override
    public T update(Object model) {
        Optional<ClientEntity> clEntity = Optional.empty();

        if (model instanceof ClientUpdateDto) {
            clEntity = clientRepository.findById(((ClientUpdateDto) model).getId());
            if(clEntity.isEmpty()) {
                logger.error("Error registro no encontrado.");
                return (T) BasicResponse.builder()
                        .code(404)
                        .status("Not found")
                        .message("Error registro no encontrado")
                        .build();
            }


            try {
                clEntity.get().setDni(((ClientUpdateDto) model).getDni());
                clEntity.get().setNames(((ClientUpdateDto) model).getNames());
                clEntity.get().setAddress(((ClientUpdateDto) model).getAddress());
                clEntity.get().setAge(((ClientUpdateDto) model).getAge());
                clEntity.get().setTelephoneNumber(((ClientUpdateDto) model).getTelephoneNumber());

                clEntity.get().setPassword(((ClientUpdateDto) model).getPassword());
                clEntity.get().setStatus(((ClientUpdateDto) model).getStatus());
                clientRepository.save(clEntity.get());
                logger.info("Registro actalizado correctamente.");
                return (T) ResponseWithObject.builder()
                        .code(200)
                        .status("Ok")
                        .message("Registro actualizado correctamente")
                        .object(clEntity.get())
                        .build();
            } catch (Exception e) {
                logger.error("Error al ingresar.");
                return (T) BasicResponse.builder()
                        .code(400)
                        .status("Error")
                        .message("Error: " + e.getMessage())
                        .build();
            }

        } else {
            logger.error("Error al no estructura.");
            return (T) BasicResponse.builder()
                    .code(400)
                    .status("Error")
                    .message("Error al registrar")
                    .build();
        }
    }

    @Override
    public T delete(Long id) {
        Optional<ClientEntity> clEntity = clientRepository.findById(id);
        if (clEntity.isEmpty()) {
            return (T) BasicResponse.builder()
                    .code(404)
                    .message("Error registro no encontrado")
                    .status("Not found")
                    .build();
        }


        try {
            clEntity.get().setStatus(false);
            clientRepository.save(clEntity.get());

            return (T) BasicResponse.builder()
                    .code(200)
                    .message("registro eliminado correctamente")
                    .status("Ok")
                    .build();


        } catch (Exception e) {
            logger.error("Error al actualizar");
            return (T) BasicResponse.builder()
                    .code(400)
                    .message("Error al eliminar")
                    .status("Error")
                    .build();
        }
    }

    @Override
    public Object findById(Long id) {
        return null;
    }

    @Override
    public List<PersonModel> findAll() {
        return null;
    }

    @Override
    public T findByNames(String names) {
        Optional<PersonEntity> prEntity = personRepository.findByNames(names);
        if(prEntity.isEmpty()) {
            return (T) BasicResponse.builder()
                    .message("Cliente no encontrado")
                    .code(404)
                    .status("Not found")
                    .build();
        }

        return (T) ResponseWithObject.builder()
                .message("Cliente encontrado")
                .code(200)
                .status("Ok")
                .object(PersonModel.builder()
                        .id(prEntity.get().getId())
                        .dni(prEntity.get().getDni())
                        .names(prEntity.get().getNames())
                        .address(prEntity.get().getAddress())
                        .age(prEntity.get().getAge())
                        .telephoneNumber(prEntity.get().getTelephoneNumber())
                        .build())
                .build();


    }

}
