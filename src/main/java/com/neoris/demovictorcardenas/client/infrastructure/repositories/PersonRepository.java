package com.neoris.demovictorcardenas.client.infrastructure.repositories;

import com.neoris.demovictorcardenas.client.infrastructure.entities.ClientEntity;
import com.neoris.demovictorcardenas.client.infrastructure.entities.PersonEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PersonRepository extends JpaRepository<PersonEntity, Long> {

    @Query("FROM PersonEntity pe " +
            "WHERE pe.names = :names")
    public Optional<PersonEntity> findByNames(@Param("names") String names);


}
