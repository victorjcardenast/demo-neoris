package com.neoris.demovictorcardenas.client.infrastructure.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Person", schema = "neoris")
@Inheritance(strategy = InheritanceType.JOINED)
public class PersonEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 13, nullable = false, unique = true)
    private String dni;

    @Column(length = 50, nullable = false)
    private String names;

    @Column
    private Integer age;

    @Column(length = 200)
    private String address;

    @Column(name = "telephone_number", length = 12)
    private String telephoneNumber;


}
