package com.neoris.demovictorcardenas.client.infrastructure.controllers;

import com.neoris.demovictorcardenas.client.application.dto.BasicResponse;
import com.neoris.demovictorcardenas.client.application.dto.ClientSaveDto;
import com.neoris.demovictorcardenas.client.application.dto.ClientUpdateDto;
import com.neoris.demovictorcardenas.client.application.dto.ResponseWithObject;
import com.neoris.demovictorcardenas.client.domain.models.ClientModel;
import com.neoris.demovictorcardenas.client.domain.services.ClientCrudService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Tag(name = "Clientes", description = "Crud Clientes")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/client")
public class ClientController {

    @Autowired
    private ClientCrudService personService;

    @PostMapping("/save")
    public ResponseEntity save(@RequestBody ClientSaveDto user) {
        var response = personService.save(user);
        return ResponseEntity.status(((BasicResponse)response).getCode()).body(response);
    }

    @PutMapping("/update")
    public ResponseEntity<?> update(@RequestBody ClientUpdateDto client) {
        var response = personService.update(client);
        if(response instanceof ResponseWithObject) {
            return ResponseEntity.status(((ResponseWithObject) response).getCode())
                    .body(response);
        }
        return ResponseEntity.status(((BasicResponse) response).getCode()).body(response);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        var response = personService.delete(id);
        return ResponseEntity.status(((BasicResponse) response).getCode()).body(response);
    }

    @GetMapping("/findbynames/{names}")
    public ResponseEntity<?> findByNames(@PathVariable String names) {
        var response = personService.findByNames(names);
        if(response instanceof ResponseWithObject) {
            return ResponseEntity.status(((ResponseWithObject) response).getCode())
                    .body(response);
        }
        return ResponseEntity.status(((BasicResponse) response).getCode()).body(response);
    }



}
