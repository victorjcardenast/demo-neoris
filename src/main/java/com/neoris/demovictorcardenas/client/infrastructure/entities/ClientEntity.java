package com.neoris.demovictorcardenas.client.infrastructure.entities;

import jakarta.persistence.*;
import lombok.*;

@Data
//@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "client_db", schema = "neoris")
@EqualsAndHashCode(callSuper=false)
public class ClientEntity extends PersonEntity{

    @Column(length = 20)
    private String name;

    @Column(length = 200)
    private String password;

    @Column
    private Boolean status;

    public ClientEntity(
            String dni,
            String names,
            String address,
            String telephoneNumber,
            String name,
            String password
    ) {
        super();
        this.setDni(dni);
        this.setNames(names);
        this.setAddress(address);
        this.setTelephoneNumber(telephoneNumber);
        this.name = name;
        this.password = password;
        this.status = true;

    }

    public ClientEntity(
            Long id,
            String dni,
            String names,
            String address,
            String telephoneNumber,
            String name,
            String password,
            Boolean status
    ) {
        super();
        this.setId(id);
        this.setDni(dni);
        this.setNames(names);
        this.setAddress(address);
        this.setTelephoneNumber(telephoneNumber);
        this.name = name;
        this.password = password;
        this.status = status;

    }

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "person_id")
//    @ToString.Exclude
//    private PersonEntity person;
}
