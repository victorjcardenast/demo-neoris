# Demo NEORIS



## Proyecto Demo NEORIS

Proyecto Demo para NEORIS

### Este proyecto tiene como objetivo mostrar la funcionalidad de Sprig Boot
### Utilitarios previos
```
# Con JDK 17 minimo
# Maven como gestor de proyectos
# Base de datos Postgres V 11 minimo
# Intellij
# Utiliza Gitlab como repositorio 
# SWAGGER V3
```
## Pasos
### 1. Creacion credenciales Base de Datos
Este demo utiliza Postgres
```
1. Creamos usuario neoris-demo con clave neoris-demo y con privilegios creacion DB
2. Creamos base de datos neorisdb y atamos al nuevo usuario
3. Creamos schema neoris indispensable para creación de tablas
   script scriptDB.sql en directorio resources del proyecto
```
### 2. Clonamos el repositorio en el directorio a su eleccion con el comando
```
cd directorioproyecto
Abrimos gitbash
git init
git clone https://gitlab.com/victorjcardenast/demo-neoris
```
### 3. Ejecutamos Intellij y abrimos el proyecto
```
Esperamos a que GRADLE actualice el proyecto
Ejecutamos el archivo DemoVictorCardenasApplication.java para inicializar le DEMO.
La aplicación tiene habilitada ddl-auto=update para la creación de todas las tablas
```
### 4. SWAGGER
```
En SWAGGER se encuentran las definiciones y los endpoint del proyecto
URL: http://localhost:8080/swagger-ui/index.html
```
